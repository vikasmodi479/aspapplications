﻿using ProductApp.MVVCore.Models;

// These Services layer is act as validation and repository layer only do action without checking validation

namespace ProductApp.MVVCore.Services
{
    public interface IProductService
    {
        List<Product> GetProducts();
        int AddProduct(Product product);
        Product? getProductById(int id);
        int DeleteProduct(int id);
        Product? DeleteProductById(int id);
    }
}
