﻿using ProductApp.MVVCore.Models;
using ProductApp.MVVCore.Repository;
using ProductApp.MVVCore.Exceptions;

namespace ProductApp.MVVCore.Services
{
    public class ProductService : IProductService
    {
        IProductRepsitory _productRepsitory;
        public ProductService(IProductRepsitory productRepsitory)
        {
            _productRepsitory = productRepsitory;
        }

        public int AddProduct(Product product)
        {
            Product? productExitResult = _productRepsitory.GetProductByName(product.Name);
            if (productExitResult == null)
            {
                return _productRepsitory.AddProduct(product);
            }
            else
            {
                throw new ProductExitsException($"{product.Name} Already Exist !!!");
            }
            
        }

        public int DeleteProduct(int id)
        {
            return _productRepsitory.DeleteProduct(id);
        }

        public Product? DeleteProductById(int id)
        {
            return _productRepsitory.DeleteProductById(id);
        }

        public Product? getProductById(int id)
        {
            return _productRepsitory.getProductById(id);
        }

        public List<Product> GetProducts()
        {
            return _productRepsitory.GetProducts();
        }
    }
}
