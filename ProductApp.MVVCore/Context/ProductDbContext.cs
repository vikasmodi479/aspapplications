﻿using Microsoft.EntityFrameworkCore;
using ProductApp.MVVCore.Models;

namespace ProductApp.MVVCore.Context
{
    public class ProductDbContext:DbContext
    {
       
        public ProductDbContext(DbContextOptions<ProductDbContext> options) : base(options)
        {
           // Database.EnsureCreated();
           //enable-migrations
           //add-migration "migration name"
           //update-database
        }
        

        public DbSet<Product> Products { get; set; }
    }
}
