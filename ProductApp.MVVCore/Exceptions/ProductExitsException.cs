﻿namespace ProductApp.MVVCore.Exceptions
{
    public class ProductExitsException : Exception
    {
        public ProductExitsException()
        {

        }
        public ProductExitsException(string msg) : base(msg)
        {

        }
    }
}
